import	{ Card, Button } from 'react-bootstrap';

export default function Course(){
	return(
		<Card>
			<Card.Body>
				<Card.Title>Computer Programming</Card.Title> 
				<Card.Text>
					<h3>Description</h3>
					<p>Lorem Ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<h4>Price</h4>
					<p>5000</p>
					<Button>Enroll</Button
				</Card.Text>
			</Card.Body>
		</Card>

	)
}